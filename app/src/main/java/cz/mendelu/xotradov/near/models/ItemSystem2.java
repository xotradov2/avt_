package cz.mendelu.xotradov.near.models;

import android.net.Uri;

/**
 * Model of system warning with two buttons
 */
public class ItemSystem2 extends ListItem {
    private String description;
    private Uri imageUri;
    private String buttonText1;
    private String buttonText2;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

    public String getButtonText1() {
        return buttonText1;
    }

    public void setButtonText1(String buttonText1) {
        this.buttonText1 = buttonText1;
    }

    public String getButtonText2() {
        return buttonText2;
    }

    public void setButtonText2(String buttonText2) {
        this.buttonText2 = buttonText2;
    }

}
