package cz.mendelu.xotradov.near.models;


import android.net.Uri;

/**
 * Model of system warning, image and text without buttons
 */
public class ItemSystem extends ListItem {
    private String description;
    private Uri imageUri;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

}
