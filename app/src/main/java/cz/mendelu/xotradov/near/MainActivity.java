package cz.mendelu.xotradov.near;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cz.mendelu.xotradov.near.models.ItemCard;
import cz.mendelu.xotradov.near.models.ItemHeadline;
import cz.mendelu.xotradov.near.models.ItemSystem;
import cz.mendelu.xotradov.near.models.ItemSystem2;
import cz.mendelu.xotradov.near.models.ListItem;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private NearAdapter nearAdapter;
    public List<ListItem> itemList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.near);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = findViewById(R.id.recycler_view);

        NestedScrollView nestedScrollView = findViewById(R.id.nested_scroll_view);
        nestedScrollView.getRootView().setBackgroundColor(getResources().getColor(R.color.colorWhiteSmoke,null));
    }

    /**
     * onResume sets up itemList with information and creates new Adapter and LayoutManager
     */
    @Override
    protected void onResume() {
        super.onResume();
        itemList.clear();
        ItemHeadline itemHeadline = new ItemHeadline();
        itemHeadline.setHeading("Poblíž");
        itemList.add(itemHeadline);

        ItemSystem listItem1 = new ItemSystem();
        listItem1.setImageUri(UriUtility.getUriToDrawable(MainActivity.this,R.drawable.ic_baseline_save_24px));
        listItem1.setDescription("Ze všech světadílů kromě Antarktidy se sjeli na dnešní Sraz století Mendelovy univerzity v Brně absolventi školy.");
        itemList.add(listItem1);

        ItemHeadline listItem2 = new ItemHeadline();
        listItem2.setHeading("Připnuto");
        itemList.add(listItem2);

        ItemCard listItem3 = new ItemCard();
        listItem3.setHeading("Upomínka!");
        listItem3.setSubheading("V 09:45 poblíž Hlavní vchod");
        listItem3.setPinned(true);
        listItem3.setDescription("Hrozn7 probl0m s sdlakfjla sdkjflasj asdfk jasdlfkja sadlkfjasl dfkj asdfsakdjf aslfkjadslf aadf asdkfj alsdfa sdfj dfg");
        listItem3.setButtonText1("Úřední hodiny studijního oddělení");
        listItem3.setButtonText2("Studijní řád");
        itemList.add(listItem3);

        ItemHeadline itemBefore = new ItemHeadline();
        itemBefore.setHeading("Dříve");
        itemList.add(itemBefore);

        itemList.add(listItem3);
        
        ItemCard listItem4 = new ItemCard();
        listItem4.setHeading("Novinka!");
        listItem4.setSubheading("V 10:45 poblíž Hlavní vchod");
        listItem4.setPinned(false);
        listItem4.setDescription("MENDELU má nově jmenovaného profesora a 9 nových docentů. V průběhu června byl v rámci akademické obce Mendelovy");
        listItem4.setButtonText1("Podívat se na profesora");
        listItem4.setButtonText2("Najít si jeho práci");
        itemList.add(listItem4);

        ItemSystem2 listItem5 = new ItemSystem2();
        listItem5.setImageUri(UriUtility.getUriToDrawable(MainActivity.this,R.drawable.ic_baseline_pan_tool_24px));
        listItem5.setDescription("Zakázali jste oznámení. Díky oznámení se obsah na chytrém místě zobrazí, aniž byste museli otevírat aplikaci.");
        listItem5.setButtonText1("Nastavení");
        listItem5.setButtonText2("Ne, děkuji");
        itemList.add(listItem5);


        nearAdapter = new NearAdapter();
        RecyclerView.LayoutManager layoutManager
                = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(nearAdapter);
    }


    public void makeToast(View view) {
        Toast.makeText(this, "Toast text", Toast.LENGTH_SHORT).show();
    }

    /**
     * Adapter class of a recyclerView.
     */
    public class NearAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        /**
         * Creates viewHolder for every viewType
         * @param viewGroup extends View implements ViewParent, ViewManager
         * @param viewType number specific for every view type, 0 - heading, 1 - system,
         *                 2 - card, 3 - system with two buttons
         * @return Newly created RecyclerView.ViewHolder based on itemView from LayoutInflater
         */
        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            switch (viewType) {
                case 0:
                    View itemView = LayoutInflater
                            .from(MainActivity.this)
                            .inflate(R.layout.row_headline,
                                    viewGroup,
                                    false);
                    return new ViewHolder0(itemView);
                case 1:
                    View itemView1 = LayoutInflater
                            .from(MainActivity.this)
                            .inflate(R.layout.row_system,
                                    viewGroup,
                                    false);
                    return new ViewHolder1(itemView1);
                case 3:
                    View itemView3 = LayoutInflater
                            .from(MainActivity.this)
                            .inflate(R.layout.row_system_2,
                                    viewGroup,
                                    false);
                    return new ViewHolder3(itemView3);
                default:
                    View itemView2 = LayoutInflater
                            .from(MainActivity.this)
                            .inflate(R.layout.row_card,
                                    viewGroup,
                                    false);
                    return new ViewHolder2(itemView2);
            }

        }

        /**
         * onBindViewHolder binds param viewHolder with item from itemList based on param position
         * @param viewHolder @NonNull RecyclerView.ViewHolder, different for every model
         * @param position position of item binded in listItem
         */
        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
            ListItem listItem = itemList.get(position);
            switch (viewHolder.getItemViewType()) {
                case 0:
                    ViewHolder0 viewHolder0 = (ViewHolder0)viewHolder;
                    ItemHeadline itemHeadline = (ItemHeadline)listItem;
                    viewHolder0.headlineTV.setText(itemHeadline.getHeading());
                    break;
                case 1:
                    ViewHolder1 viewHolder1 = (ViewHolder1)viewHolder;
                    ItemSystem itemSystem = (ItemSystem)listItem;
                    viewHolder1.systemIV.setImageURI(itemSystem.getImageUri());
                    viewHolder1.systemTV.setText(itemSystem.getDescription());
                    break;
                case 2:
                    final ViewHolder2 viewHolder2 = (ViewHolder2)viewHolder;
                    ItemCard itemCard = (ItemCard)listItem;
                    viewHolder2.cardHeading.setText(itemCard.getHeading());
                    viewHolder2.cardPin.setImageURI(UriUtility.getUriToDrawable(MainActivity.this,R.drawable.ic_baseline_lock_open_24px));
                    if (itemCard.isPinned()){
                        //viewHolder2.cardPin.setImageURI(UriUtility.getUriToDrawable(MainActivity.this,R.drawable.ic_baseline_lock_open_24px));
                        //viewHolder2.cardPin.setBackgroundColor(Color.CYAN);
                        viewHolder2.cardPin.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.round_shape_btn));
                        viewHolder2.cardPin.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPurple), android.graphics.PorterDuff.Mode.SRC_IN);
                    }else {
                        //viewHolder2.cardPin.setImageURI(UriUtility.getUriToDrawable(MainActivity.this,R.drawable.ic_baseline_save_24px));
                        //viewHolder2.cardPin.setBackgroundColor(Color.GRAY);
                        viewHolder2.cardPin.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.round_shape_btn_purple));
                        viewHolder2.cardPin.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                    }
                    viewHolder2.cardPin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int p = viewHolder2.getAdapterPosition();
                            ItemCard itemCard1 = (ItemCard)MainActivity.this.itemList.get(p);
                            boolean pinned = itemCard1.isPinned();
                            itemCard1.setPinned(!pinned);
                            if (!pinned){
                                //viewHolder2.cardPin.setImageURI(UriUtility.getUriToDrawable(MainActivity.this,R.drawable.ic_baseline_lock_open_24px));
                                //viewHolder2.cardPin.setBackgroundColor(Color.CYAN);
                                viewHolder2.cardPin.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.round_shape_btn));
                                viewHolder2.cardPin.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPurple), android.graphics.PorterDuff.Mode.SRC_IN);

                            }else {
                                //viewHolder2.cardPin.setImageURI(UriUtility.getUriToDrawable(MainActivity.this,R.drawable.ic_baseline_save_24px));
                                //viewHolder2.cardPin.setBackgroundColor(Color.GRAY);
                                viewHolder2.cardPin.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.round_shape_btn_purple));
                                viewHolder2.cardPin.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                            }
                        }
                    });
                    viewHolder2.cardSubHeading.setText(itemCard.getSubheading());
                    viewHolder2.cardDescription.setText(itemCard.getDescription());
                    viewHolder2.cardButton1.setText(itemCard.getButtonText1());
                    viewHolder2.cardButton2.setText(itemCard.getButtonText2());
                    break;
                case 3:
                    ViewHolder3 viewHolder3 = (ViewHolder3)viewHolder;
                    ItemSystem2 itemSystem2 = (ItemSystem2)listItem;
                    viewHolder3.systemIV.setImageURI(itemSystem2.getImageUri());
                    viewHolder3.systemTV.setText(itemSystem2.getDescription());
                    viewHolder3.systemButton1.setText(itemSystem2.getButtonText1());
                    viewHolder3.systemButton2.setText(itemSystem2.getButtonText2());
                    break;
            }
        }

        /**
         * @return number of items in item list
         */
        @Override
        public int getItemCount() {
            return itemList.size();
        }

        /**
         * View Holder of a simple headline composed of just one Text View
         */
        class ViewHolder0 extends RecyclerView.ViewHolder {
            public TextView headlineTV;
            public ViewHolder0(@NonNull View itemView){
                super(itemView);
                headlineTV = itemView.findViewById(R.id.row_headline_text_view);
            }
        }

        /**
         * View Holder of a simple system warning, composed of ImageView and TextView
         */
        class ViewHolder1 extends RecyclerView.ViewHolder {
            public ImageView systemIV;
            public TextView systemTV;
            public ViewHolder1(View itemView){
                super(itemView);
                systemIV = itemView.findViewById(R.id.row_system_image_view);
                systemTV = itemView.findViewById(R.id.row_system_text_view);
            }
        }

        /**
         * View Holder of a full card notice, composed of heading, subheading, long description,
         * two Text Buttons and ImageButton of pin
         */
        class ViewHolder2 extends RecyclerView.ViewHolder {
            public TextView cardHeading;
            public ImageView cardPin;
            public TextView cardSubHeading;
            public TextView cardDescription;
            public Button cardButton1;
            public Button cardButton2;
            public ViewHolder2(View itemView){
                super(itemView);
                cardHeading =itemView.findViewById(R.id.card_headline);
                cardPin = itemView.findViewById(R.id.card_pin);
                cardSubHeading = itemView.findViewById(R.id.card_subheading);
                cardDescription = itemView.findViewById(R.id.card_description);
                cardButton1 = itemView.findViewById(R.id.card_button_1);
                cardButton2 = itemView.findViewById(R.id.card_button_2);
            }
        }

        /**
         * View Holder of a system warning with two Text Buttons
         */
        class ViewHolder3 extends RecyclerView.ViewHolder {
            public ImageView systemIV;
            public TextView systemTV;
            public Button systemButton1;
            public Button systemButton2;
            public ViewHolder3(View itemView){
                super(itemView);
                systemIV = itemView.findViewById(R.id.row_system_2_image_view);
                systemTV = itemView.findViewById(R.id.row_system_2_text_view);
                systemButton1 = itemView.findViewById(R.id.row_system_2_button_1);
                systemButton2 = itemView.findViewById(R.id.row_system_2_button_2);
            }
        }


        /**
         * Determines item type based on position in listItem
         * @param position Position of item in itemList
         * @return int of viewType, default is 0
         */

            public int getItemViewType(int position) {

                ListItem listItem = itemList.get(position);
                if(listItem instanceof ItemSystem){
                    return 1;
                }

                if (listItem instanceof ItemSystem2){
                    return 3;
                }

                if (listItem instanceof  ItemCard){
                    return 2;
                }

                return 0;


            }


        }
}
