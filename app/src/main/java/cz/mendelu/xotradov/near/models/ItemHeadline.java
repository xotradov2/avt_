package cz.mendelu.xotradov.near.models;

/**
 * Model of a simple headline.
 */
public class ItemHeadline extends ListItem{
    private String heading;

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }
}
